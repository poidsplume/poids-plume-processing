#!/bin/bash
# 
# Commands that are run after a website update. 
# 
set -euf -o pipefail

# We load the secrets file which is not checked in git
SECRET_FILE="$(dirname "$(realpath "$0")")/scripts.secrets"
if [ -e "$SECRET_FILE" ]; then 
  source "$SECRET_FILE"
else 
  echo "No secrets provided." 
  exit 1
fi

if [ "$(echo "$RUN_ON_HOSTS" | grep -c "$(hostname)")" == "0" ]; then 
  echo "This script should not run on the current host"
  exit 0
fi




# Send data to server 
# rsync "./web_output/" "$REMOTE_USER@$REMOTE_SERVER:$REMOTE_FOLDER" \
#   -avzz --no-perms \
#   --info=progress2 

# Unmount target directory
# echo "Unmounting target directory"
# sudo umount -l "./web_output"

# Set correct permissions on the remote server 
# Note: there is no need for this is the primary group of $REMOTE_USER 
# is www-data. 
# echo "Fixing perms on remote server..."
# ssh "$REMOTE_ADMIN_USER@$REMOTE_SERVER" "chown -R $REMOTE_USER:www-data $REMOTE_FOLDER"
# ssh "$REMOTE_ADMIN_USER@$REMOTE_SERVER" "chmod -R 750 $REMOTE_FOLDER"


# Send email to admins
echo "Sending email to admins..."
USER_URLENC="$(echo "urlcodec encode $SMTP_USER" | mailx -#)"
PASS_URLENC="$(echo "urlcodec encode $SMTP_PASS" | mailx -#)"
STATUS_MESSAGE="OK"

# We send the log file if it is there 
if [ -e "last_render.log" ]; then 
  MAIL_ATTACH_ARG="-a last_render.log"
else 
  MAIL_ATTACH_ARG=""
  STATUS_MESSAGE="WARNING: Could not find log file."
fi

# Close ssh tunnel to server holding mysql database 
# echo "Closing ssh tunnel to db server..."
# if [ -e "$SSH_PIDFILE" ]; then 
#   kill -p "$(cat "$SSH_PIDFILE")"
#   rm "$SSH_PIDFILE"
# else 
#   STATUS_MESSAGE="$STATUS_MESSAGE \nWARNING: Could not find $SSH_PIDFILE"
# fi
# shellcheck disable=SC2086
echo -e "Mise à jour du site terminée ($(date)). \n Status: $STATUS_MESSAGE" | \
  mailx \
  -r "$MAILFROM_ADDRESS" \
  -S v15-compat \
  -S smtp-use-starttls \
  -S "mta=smtp://$USER_URLENC:$PASS_URLENC@$SMTP_SERVER:$SMTP_PORT" \
  -s "[website-rendering] Mise à jour du site terminée $(date)" \
  $MAIL_ATTACH_ARG \
  "$MAILTO_ADDRESS"

# Cleanup render log 
if [ -e "last_render.log" ]; then 
  rm "last_render.log"
fi
