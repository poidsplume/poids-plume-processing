#!/bin/bash
# 
# set -euf -o pipefail

# Minimum time between updates
MINTIME=$(( 60 * 30 )) # 30 minutes

while true; do 
  
  # Update code
  echo "Pulling software updates"
  git pull --no-edit 
  
  # Launch an update
  echo "Launching website update ($(date))"
  BEFOREDATE="$(date +%s)"
  
  # Get number of cores
  if [ -z "$1" ]; then 
    ncores="$(grep -c processor /proc/cpuinfo)"
  else 
    ncores="$1"
  fi
  
  # Get number of tasks to do at once
  if [ -z "$2" ]; then 
    maxn="NULL"
  else 
    maxn="$2"
  fi
  
  # If we are between 12pm and 2am, do db maintenance
  if [ "$(date +H)" -lt "2" ]; then 
    R -q --vanilla -e "\
    pkgbuild::with_debug(devtools::load_all(), debug = FALSE); \
    do_database_maintenance()" 
  fi
  
  # Launch website update
  R -q --vanilla -e "\
    pkgbuild::with_debug(devtools::load_all(), debug = FALSE); \
    update_all_timeout(workers = $ncores, maxn = $maxn, .timeout = 4*3600)" 
  
  # Sleep if needed
  runtime="$(( $(date +%s) - BEFOREDATE ))"
  if [ $runtime -lt $MINTIME ]; then 
    sleep $(( MINTIME - runtime ))
  fi
  
  # Kill all remaining R sessions
#   echo "Killing all remaining R sessions"
#   pkill -P "$ID"
  
done
