# 
# Functions that carry out some maintenance on the data/databases
# 

# These functions will take care of upgrading the db if needed 
upgrade_db <- function() { 
  add_missing_video_ids()
  make_all_feeders_lowercase()
  cleanup_old_db_exports()
  return(TRUE)
}

add_missing_video_ids <- function() { 
  
  # Get the rows in videotags table where the video_id is null
  con <- db_connect()
  query <- dbSendQuery(con, "SELECT * FROM videotags WHERE video_id IS NULL OR video_id='';")
  alltags <- dbFetch(query)
  dbClearResult(query)
  
  # Filter alltags table
  alltags <- unique(alltags[ ,c("video_datetime", "feeder", "video_id", 
                                "video_name")])
  alltags[ ,'year']   <- year(alltags[ ,"video_datetime"])
  alltags[ ,'month']  <- month(alltags[ ,"video_datetime"])
  alltags[ ,'day']    <- day(alltags[ ,"video_datetime"])
  alltags[ ,'feeder']    <- tolower(alltags[ ,"feeder"])
  
  # Load allbirds and compute sha1s from video paths 
  allbirds <- data.frame(vid = findall(RAWDATA_LOCALFOLDER, "^.*h264$"))
  allbirds <- data.frame(allbirds, 
                         parse_path(dirname(dirname(allbirds$vid)), 
                                    spec = RAWDATA_PATHSPEC, 
                                    spectype = RAWDATA_SPECTYPE))
  allbirds <- mutate(allbirds, 
                     video_name = rm_ext(basename(vid)))

  merg <- join(alltags, 
               allbirds[ , c("feeder", "year", "month", "day",
                             "video_name", "vid")], 
               by = c("feeder", "year", "month", "day", "video_name"), 
               type = "left", match = "first")
  merg <- subset(merg, !is.na(vid))
  
  # Add ids
  merg[ ,"new_video_id"] <- map_chr(merg[ ,"vid"], digest::sha1)
  
  if ( nrow(merg) > 0 ) { 
    cat("Updating video ids...") 
    for ( i in seq.int(nrow(merg)) ) { 
      cat('.')
      dat <- with(merg[i, ], 
                  as.Date(p(year, "-", lzero(month), "-", lzero(day)), 
                          format = "%Y-%m-%d"))
      
      req <- p("UPDATE videotags SET video_id = '", merg[i, "new_video_id"], "' ", 
              "WHERE video_date = '", dat, "' AND ", 
              "feeder = '", fmaj(merg[i, "feeder"]), "' AND ", 
              "video_name = '", fmaj(merg[i, "video_name"]), "';")
      query <- dbSendQuery(con, req)
      dbClearResult(query)
    }
    cat("\n")
  } else { 
    cat("No video ids to update\n")
  }
  
  db_disconnect(con)

  return(TRUE)
}

make_all_feeders_lowercase <- function() { 
  con <- db_connect()
  req <- p("UPDATE videotags SET feeder = LOWER(feeder);")
  query <- dbSendQuery(con, req)
  db_disconnect(con)
  return(TRUE)
}

# Resize raw video above a certain size
recompress_vids <- function() { 
  allvids <- findall(RAWDATA_LOCALFOLDER, name = "^.*h264$")
  allvids <- rev(allvids) 
  
  l_ply(allvids, function(v) { 
    cat("\n", v, "\n")
    out <- video_dims(v)
    osize <- file.size(v)
    # 616 is the height in fringilla + poecile 
    # We skip the video if it's already pretty small (less than 1M)
    if ( (osize/1e6 > 1) && out[ ,"height"] > 0 ) { 
      new_video_size <- min(out[ ,'height'], 720)
      newfile <- resize_one_vid(data.frame(vid = v), outdir = "/tmp/", 
                                new_ext = "h264", new_height = new_video_size, 
                                overwrite = TRUE)
      
      nsize <- file.size(newfile)
      cat("  Reduced file size by ", 
          round( (1 - nsize / osize) * 100, 2), "% (",
          round(osize/1e6, 2), "MB -> ", 
                round(nsize/1e6, 2), "MB)\n", 
          sep = "")
      
      # Transfer file back if this did reduce file size by more than 70%
      if ( (!is.na(nsize)) && (nsize / osize) < (1 - .7) ) { 
        file.copy(newfile, v, overwrite = TRUE)
      } else { 
        cat("  Not enough size change, not copying\n")
      }
      
      file.remove(newfile)
    } 
  }, .progress = "text")
  
}

video_dims <- function(v) { 
  
  out <- system(p("ffprobe -v error -select_streams v:0 -show_entries ", 
                  "stream=width,height -of csv=s=x:p=0 ", v), 
                intern = TRUE)
  out <- strsplit(out, "x")[[1]]
  out <- as.data.frame(as.list(as.numeric(out)))
  names(out) <- c('width', 'height')
  out
}

cleanup_old_db_exports <- function() {   
  # Cleanup old database files 
  alldbs <- dir(WEBOUTPUT_LOCALFOLDER, pattern = "^pp_database_.*", full = TRUE)
  mtimes <- file.info(alldbs)[ ,"mtime"]
  to_delete <- alldbs[order(mtimes)][1:(length(alldbs)-1)]
  file.remove(to_delete)
}

