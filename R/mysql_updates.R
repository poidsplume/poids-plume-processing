# 
# 
# 

# Update the pagedata table
update_page_data <- function(datadf) { 
  con <- db_connect()
  
  statements <- lapply(seq.int(nrow(datadf)), function(i) { 
    vals <- sqlf(datadf[i, ])
    stopifnot( all(c("pagetype", "feeder", "date", "category", 
                     "datakey") %in% names(vals)) )
    p("REPLACE INTO ", DBPAGEDATTBL, " ", 
      "(", p(names(vals), collapse = ", "), ") ", 
      "VALUES ", 
      "(", p(vals, collapse = ", "), ")", 
      ";")
  })
  
  for ( i in seq_along(statements) ) { 
    if ( VERBOSE > 50 ) { 
      cat(statements[[i]], "\n")
    }
    dbExecute(con, statements[[i]])
  }
  
  db_disconnect(con)
  return(TRUE)
}

# Update the feeder table
update_feeder_data <- function(datadf) { 
  
  if ( ! "feeder" %in% names(datadf) ) { 
    stop("Unable to update feeder data without feeder name")
  }
  
  con <- db_connect()
  
  cols_to_update <- names(except(datadf, "feeder"))
  
  # Check if feeder exists, if not we will need to create it 
  current_feeder <- datadf[ ,"feeder"]
  sql <- p("SELECT * FROM ", DBFEEDERTBL, " WHERE feeder = ?")
  query <- dbSendQuery(con, sql)
  dbBind(query, list(current_feeder))
  feeder_line <- dbFetch(query)
  dbClearResult(query)
  
  if ( nrow(feeder_line) == 0 ) { 
    cat("%%%%%%%%%%%%%%%%%%\n")
    cat("%\n")
    cat("CREATING FEEDER IN FEEDERS DB: ", current_feeder, "\n", sep = "")
    cat("%\n")
    cat("%%%%%%%%%%%%%%%%%%\n")
    # Create a new feeder with default values
    sql <- p("INSERT INTO ", DBFEEDERTBL, " ", 
             "(name, family_name, username, feeder, policy, `from`, `to`)", " ", 
            "VALUES (?,?,?,?,?,?,?)")
    query <- dbSendQuery(con, sql)
    now <- Sys.Date()
    now_plus_one_year <- Sys.Date()
    year(now_plus_one_year) <- year(now_plus_one_year) + 1
    dbBind(query, list("unknown_name", 
                       "unknown_family_name",
                       "unknown_username", 
                       current_feeder, 
                       1, # default policy: shared to association
                       Sys.Date(), 
                       now_plus_one_year
                       ))
    dbClearResult(query)
  }
  
  statements <- lapply(seq.int(nrow(datadf)), function(i) { 
    p("UPDATE ", DBFEEDERTBL, " ", 
      "SET ", format_tbl(datadf[i,cols_to_update]), " ", 
      "WHERE feeder = ", sqlstr(datadf[i,"feeder"]), 
      ";")
  })
  
  
  for (i in seq_along(statements)) { 
    if ( VERBOSE > 50 ) { 
      cat(statements[[i]], "\n")
    }
    dbExecute(con, statements[[i]])
  }
  
  db_disconnect(con)
  return(TRUE)
}

# Update the bird database 
update_bird_species_db <- function() { 
  con <- db_connect()
  
  # Copy bird icons
  prefix <- fpath("bird_icons")
  local_dir <- fpath(WEBGEN_LOCALFOLDER, prefix)
  remote_dir <- fpath(WEBGEN_REMOTESUBFOLDER, prefix)
  allbirds <- dir(fpath(WEBDATA_LOCALFOLDER, "static/bird_species"), 
                  full = TRUE, pattern = "*.png")
  
  if ( ! dir.exists(local_dir) ) { 
    dir.create(local_dir, recursive = TRUE)
  }
  file.copy(allbirds, local_dir, overwrite = TRUE)
  
  # Adjust and create remote database
  remote_paths <- map_chr(BIRDDB[ ,"code"], 
                          function(n) fpath("/", remote_dir, n))
  BIRDDB[ ,"icon"] <- remote_paths
  
  # Replace the table with the one with new species 
  if ( DBSPECIESTBL %in% dbListTables(con) ) { 
    dbRemoveTable(con, DBSPECIESTBL)
  }
  dbCreateTable(con, DBSPECIESTBL, BIRDDB)
  dbWriteTable(con, DBSPECIESTBL, BIRDDB, overwrite = TRUE)
  
  db_disconnect(con)
  return(TRUE)
}

format_tbl <- function(tbl) { 
  if ( nrow(tbl) > 1 ) { 
    stop("This function can only format one row at a time")
  }
  li <- Map(function(n, o) p(n, "=", sqlf(o)), names(tbl), tbl)
  paste(unlist(li), collapse = ", ")
}

# Format data 
sqlf <- function(x, ...) { 
  if ( length(x) > 1 ) { 
    return( unlist(lapply(x, sqlf)) )
  }
  if ( is.na(x) ) { 
    return(NULL)
  }
  switch(class(x)[1], 
         character = sqlstr(x), 
         logical   = sqlbool(x), 
         integer   = sqlint(x, ...), 
         numeric   = sqldouble(x, ...), 
         POSIXct   = sqldatetime(x, ...), 
         Date      = sqldate(x, ...), 
         stop("Format unknown:"))
}

sqlstr <- function(s) { 
  p("'", s, "'")
}
sqldouble <- function(d, digits = 2) { 
  return( sprintf(p("%.", digits, "f"), d) )
}
sqlbool <- function(b) { 
  as.integer(b)
}
sqldatetime <- function(d) { 
  print("datetime not implemented")
  browser()
}
sqldate <- function(d) { 
  p("'", as.character(d), "'")
}

sqlint <- function(i) { 
  sprintf("%i", round(i))
}
