# 
# This function will update the video database 
# 

update_video_database <- function(workers = WORKERS, maxn = NULL, ...) { 
  renderer(get_processed_table(), 
           fun = update_video_database_one, 
           message = expression(p('Updating video db: ', folder, '\n')), 
           workers = workers, 
           maxn = maxn, 
           ...)
  return(TRUE)
}

update_video_database_one <- function(folder_df, 
                                      update_existing = UPDATE_EXISTING_VID) { 
  datpath <- fpath(folder_df[ ,"folder"], "birdvids.fdat")
  birdvids <- read_featherdf(datpath)
  
  # If birdvids is not a proper data.frame, bail
  if ( ! okdf(birdvids) ) { 
    return(TRUE)
  }
  
  con <- db_connect()
  
  # Request the data for that video
  file_batch_size <- 100
  files_to_send <- character(0)
  for ( l in seq.int(nrow(birdvids)) ) { 
    a <- birdvids[l, ]
    
    # TODO: remove these once processing is updated
    if ( ! "birdweight_sd" %in% names(a) ) { 
      if ( "birdweight.sd" %in% names(a) ) { 
        a$birdweight_sd <- a$birdweight.sd
      } 
      if ( "birdweight.ci" %in% names(a) ) { 
        a$birdweight_sd <- a$birdweight.ci
      }
    }
    if ( ! "totaltime" %in% names(a) ) { 
      a$totaltime <- NA
    }
    if ( ! "asym" %in% names(a) ) { 
      a$asym <- NA
    }
    if ( ! "mean_lum" %in% names(a) ) { 
      a$mean_lum <- NA
    }
    if ( ! "fingerprint" %in% names(a) ) { 
      a$fingerprint <- NA
    }
    
    
    # Check if the video is already there in the db 
    sql <- p("SELECT * from ", DBVIDEOSTBL, " WHERE video_id = '", 
            a[ ,"video_id"], "'")
    req <- dbSendQuery(con, sql)
    dat <- dbFetch(req)
    dbClearResult(req) 
    
    if ( nrow(dat) == 0 || update_existing ) { 
      
      outdir_prefix <- fpath("/generated/videos", a$feeder, 
                             substr(a$video_id, 1, 1), a$video_id)
      outdir_local <- fpath(WEBOUTPUT_LOCALFOLDER, outdir_prefix)
      
      if ( ! dir.exists(outdir_local) ) { 
        dir.create(outdir_local, recursive = TRUE) 
      }
        
      # Make the resized video, the weight graph and the thumbnail
      resize_one_vid(a, 
                     outpath = fpath(WEBOUTPUT_LOCALFOLDER,
                                     outdir_prefix, "video.mp4"))
      make_weight_plots(a, 
                        outpath = fpath(WEBOUTPUT_LOCALFOLDER, 
                                        outdir_prefix, "weight_plot.png"))
      make_video_thumbnail(a, 
                           outpath = fpath(WEBOUTPUT_LOCALFOLDER, 
                                           outdir_prefix, "video.jpg"))
      
      # Send the files. Here we sync the whole directory, which is OK with 
      # send_files
      files_to_send <- c(files_to_send, outdir_prefix)
      
      # We sync files when we reach the last video, or every file_batch_size
      if ( l == nrow(birdvids) || 
          length(files_to_send) > file_batch_size ) { 
        ok_files <- send_files(files_to_send, expand = TRUE)
        files_to_send <- character(0)
        # Stop here if we could not send the files
        if ( ! ok_files ) { 
          cat("Could not send files :( Retry later?")
          return(FALSE)
        }
      }
    }
    
    # There is an existing video in the db with that id: update it
    if ( nrow(dat) > 0 ) { 
      if ( update_existing ) { 
        sql <- p("UPDATE ", DBVIDEOSTBL, " SET ", 
                 "local_path = ?, ", 
                 "date = ?, ", 
                 "datetime = ?, ", 
                 "thumbnail = ?, ", 
                 "vidfile = ?, ", 
                 "birdweight = ?, ", 
                 "birdweight_sd = ?, ", 
                 "wgtgraph = ?, ", 
                 "duration = ?, ", 
                 "samples = ?, ", 
                 "okweight = ?, ", 
                 "mov_index = ?, ", 
                 "wgtvar = ?, ", 
                 "asym = ?, ", 
                 "mean_lum = ?, ", 
                 "fingerprint = ?, ", 
                 "model_hasbird_pred = ?, ", 
                 "model_species_pred = ?, ", 
                 "species_auto = ?", 
                 " ", "WHERE video_id = ?")
        req <- dbSendQuery(con, sql)
        
        dbBind(req, with(a, list(vid, 
                                 as.Date(num_to_posixt(time)), 
                                 num_to_posixt(time), 
                                 fpath(outdir_prefix, "video.jpg"), 
                                 fpath(outdir_prefix, "video.mp4"), 
                                 birdweight, 
                                 birdweight_sd, 
                                 fpath(outdir_prefix, "weight_plot.png"), 
                                 totaltime, 
                                 n, 
                                 okweight, 
                                 mov_index, 
                                 wgtvar, 
                                 asym, 
                                 mean_lum, 
                                 fingerprint, 
                                 model_hasbird_pred, 
                                 model_species_pred, 
                                 species_auto, 
                                 video_id)))
        dbClearResult(req) 
      }
      
    # There is no existing record: insert new video
    } else { 
      
      # Get owner name, family name and such
      feeder_info <- get_feeder_data(a$feeder)
      
      # Create the line for the video 
      sql <- p("INSERT INTO ", DBVIDEOSTBL, 
              "(video_id, local_path, owner_name, owner_family_name, ", 
                "feeder, date, datetime, thumbnail, ", 
                "vidfile, birdweight, birdweight_sd, wgtgraph, duration, ", 
                "samples, okweight, mov_index, wgtvar, asym, mean_lum, fingerprint, model_hasbird_pred, model_species_pred, species_auto)", " ", 
              "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")
      req <- dbSendQuery(con, sql)
      
      # Bind and execute query
      dbBind(req, with(a, 
                       list(video_id, vid, feeder_info$name, 
                            feeder_info$family_name, 
                            feeder, 
                            as.Date(num_to_posixt(time)), 
                            num_to_posixt(time), 
                            fpath(outdir_prefix, "video.jpg"), 
                            fpath(outdir_prefix, "video.mp4"), 
                            birdweight, 
                            birdweight_sd, 
                            fpath(outdir_prefix, "weight_plot.png"), 
                            totaltime, 
                            n, 
                            okweight, 
                            mov_index, 
                            wgtvar, 
                            asym, 
                            mean_lum, 
                            fingerprint, 
                            model_hasbird_pred, 
                            model_species_pred, 
                            species_auto)))
      dbClearResult(req) 
    }
  }
  
  db_disconnect(con)
  
  return(TRUE)
}

get_feeder_data <- function(feeder) { 
  con <- db_connect()
  # Get owner of feeder 
  sql <- p("SELECT name, family_name FROM ", 
            DBFEEDERTBL, " WHERE feeder = ?") 
  req <- dbSendQuery(con, sql)
  dbBind(req, list(feeder))
  poldata <- dbFetch(req)
  dbClearResult(req) 
  db_disconnect(con)
  
  return(poldata)
}


