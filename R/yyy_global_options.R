#
# Copyright (2020), le Projet Poids Plume 
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 

#  
# Global options for the feeder data processsing
# 

# Set some global options for this package
options(stringsAsFactors = FALSE)

if ( ! file.exists("./local.R") ) { 
  stop('Local information does not exist, I cannot proceed')
} else { 
  source('./local.R')
}

# Folders for feeder data
# -----------------------
# 
DATA_LOCALFOLDER      <- p(slash(RENDERING_FOLDER), "feederdat")
RAWDATA_LOCALFOLDER   <- p(slash(DATA_LOCALFOLDER), "rawdata")
PROCESSED_LOCALFOLDER <- p(slash(DATA_LOCALFOLDER), "processed")
SEASON_LOCALFOLDER    <- p(slash(DATA_LOCALFOLDER), "season")
WEBDATA_LOCALFOLDER   <- p(slash(RENDERING_FOLDER), "web")
WEBOUTPUT_LOCALFOLDER   <- p(slash(RENDERING_FOLDER), "web_output")
# Folder containing html files templates
HTMLTEMPLATES_LOCALFOLDER <- p(slash(RENDERING_FOLDER), "htmltemplates")

# Specification to parse the path of a file in a raw data folder. Each field 
# describes a folder level, starting from the end of the path and not including 
# the file name. Use __ to discard a folder level
RAWDATA_PATHSPEC <- c('day', 'month', 'year', 'feeder', '__', '__', '__')
RAWDATA_SPECTYPE <- c('num', 'num', 'num', 'str', '__', '__', '__')
PROCESSED_PATHSPEC <- c('day', 'month', 'year', 'feeder', '__', '__', '__')
PROCESSED_SPECTYPE <- c('num', 'num', 'num', 'str', '__', '__', '__')

# Options for data procMYSQLesssing 
VERBOSE <- 100L
REGEN_FORCE <- FALSE
INTERP_HZ <- 5 # A resampling is done at processing time
FEEDERTZ_DEFAULT <- "Europe/Paris"
TEMPDIR  <- '/tmp/'
CACHEDIR <- './cache'
# Videos will not be considered outside of these hours of the day
MIN_HOUR <- 5
MAX_HOUR <- 21

# Parameters of data filtering 
HUMITEMP_MEAN <- 10 * 60  # ten-minute average for humi/temp
SYS_MEAN <- 60 # average for sysload/systemp

# Bird event stuff
TIME_AROUND_EVENT <- 5 # in seconds
TIME_AROUND_EVENT_NOT_BEFORE <- 0.5 # in seconds
TIME_BEFORE_EVENT <- 2

# Video movement index threshold 
MOVINDEX_THRESH <- 1e-4 # All of them
VIDEO_OVERWRITE <- FALSE # Should we overwrite videos when they already exist ?
VIDEO_FRAMERATE <- 25 # # Default fps for raspivid is 25: we need to change it 
                      # here if that ever changes. TODO: it should be set 
                      # explicitely in the feeder software. 
VIDEO_HEIGHT <- 540
VIDEO_WIDTH <- 718
FFMPEG_THREADS <- 1 # We use 1 threads on ffmpeg as we use parallelism our way 
FFMPEG_CMD <- "ffmpeg -filter_threads 1 -threads 1 -y"
FFMPEG_TIMEOUT <- 60 # the video conversion will abort if this timeout is reached

# Parameters related to fingerprinting of the videos, which is used when 
# trying to ID birds from the video.
# Model parameters, you probably do not want to change these 
SPECTRUM_NBINS <- 24
SPECTRUM_FRAMES <- 12 

# Species model training parameters
# Keep at most this number of videos per species for training
KEEP_MAX <- 1024
# Do not consider classes with this number of videos for training
KEEP_MIN <- 128

# Hasbird model training 
# Minimum number of points for training
HASBIRD_MINIMUM_DATA <- 32

# Weight filtering option 
MAX_WEIGHT_AMPLITUDE <- 1000

# Sync options
RSYNC_CMD <- "rsync -avzz" 
SSH_CMD <- "ssh" 

# Mysql sync
# Send at most this number of sql commands 
SQL_BATCH_SIZE <- 500
# Accumulate this number of files before sending
FILE_BATCH_SIZE <- 100
# Should we update the videos that already exist in the db ? 
UPDATE_EXISTING_VID <- TRUE
# Should we overwrite the weight graphs when they exist ?
WEIGHT_PLOT_OVERWRITE <- FALSE 
# Wait till all data has been downloaded before processing
WAIT_FOR_DATA_DOWNLOAD <- TRUE 
# Timeout for when futures hang (in seconds). We bail if we could not find 
# a free core after two hours. Then it probably means there has been a stall 
# somewhere and we need to reset processing
FUTURE_WAIT_TIMEOUT <- 3600 * 2

# Page output options 
# -------------------

# Put recent months on top and old ones at the bottom
MONTHS_INVERT_ORDER <- TRUE
# Max number of days before we stop rendering the logs
LOGS_MAX_DAYS <- 30 
# Which quantile in the number of birds defines a good day at a the feeder ? 
# This is used for the gauge on the daily overview. 
GOOD_DAY_PERC <- 0.1 # top 10% 

# Set default ggplot theme
DEFAULT_GGTHEME <- theme_minimal() + 
                     theme(text = element_text(family = "AndikaBasic", 
                                               size = 8))
theme_set(DEFAULT_GGTHEME)

# Break trend line after this amount of missing days
SEASON_TREND_MAXGAP <- 2

# Number of videos to render on the homepage
HOMEPAGE_NVIDEOS <- 10

# For some annual graphs, just keep months in that interval
MIN_MONTH <- 8 
MAX_MONTH <- 6

# Access control 
# --------------

DEFAULT_POLICY <- 0 # Private is the default data access policy

# We need to set the locale to the same as the feeders to be able to parse 
# time 
Sys.setlocale("LC_TIME", "en_GB.UTF-8")

# Set default parallel processing and caching options
WORKERS <- 1
if ( ! exists('CACHE') ) { 
  CACHE <- FALSE
}

# Fail if error occurs in code. If not it will be logged to error.log
FAIL_ON_ERROR <- TRUE

# Create required directories if absent
for ( d in c(DATA_LOCALFOLDER, 
             RAWDATA_LOCALFOLDER, 
             PROCESSED_LOCALFOLDER, 
             CACHE_LOCALFOLDER, 
             SEASON_LOCALFOLDER, 
             WEBDATA_LOCALFOLDER, 
             WEBOUTPUT_LOCALFOLDER) ) { 
  dir.create(d, showWarnings = FALSE)
}

