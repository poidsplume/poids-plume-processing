#
# Copyright (2020), le Projet Poids Plume 
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
# 

# 
# This file will render overviews
# 

render_overviews <- function(workers = WORKERS, ...) { 
  
  most_recent <- ddply(get_processed_table(), ~ feeder, function(df) { 
      tail(df[with(df, order(year, month, day)), ], 1)
    })
  
  renderer(most_recent, 
           render_one_overview, 
           expression(p('Rendering overview for ', feeder, '\n')), 
           workers = workers, 
           ...)
  
  return(TRUE)
}

render_one_overview <- function(folder_df) {
  
  con <- db_connect()
  
  # Get folders/paths in which stuff will be written
  outpath <- fpath("generated/overviews", folder_df[,'feeder'])
  local_outdir <- fpath(WEBOUTPUT_LOCALFOLDER, outpath)
  if ( ! dir.exists(local_outdir) ) { 
    dir.create(local_outdir, recursive = TRUE)
  }
  
  # Make the graph for the season 
  sql <- p("SELECT birdn, species, sex, datetime, ", 
           "species_auto, model_species_pred, model_hasbird_pred FROM ",
           DBVIDEOSTBL, " ", 
           "WHERE feeder = '", folder_df[ ,"feeder"], "' AND ", 
           "model_hasbird_pred > 0.5 AND model_hasbird_pred IS NOT NULL")
  all_birds <- dbFetch(dbSendQuery(con, sql))
  all_birds <- mutate(all_birds, year = year(datetime), 
                      month = month(datetime), 
                      day   = day(datetime), 
                      spauto = ifelse(is.na(species) & model_species_pred > .9 &
                                        model_hasbird_pred > 0.5, 
                                      species_auto, species))
  if ( okdf(all_birds) ) { 
    bird_counts <- ddply(all_birds, 
                        ~ year + month + day, summarise, 
                        n = length(year), time = num_to_posixt(mean(datetime)))
  } 
  
  # Stats for today
  d <- Sys.Date()
  n_today <- 0
  if ( okdf(all_birds) ) { 
    todaybirds <- subset(bird_counts, d == as.Date(bird_counts[ ,"time"]))
    good_day_n <- quantilealt(bird_counts[ ,'n'], 1 - GOOD_DAY_PERC)
    if ( okdf(todaybirds) ) { 
      good_day_n <- round(good_day_n)
      n_today <- todaybirds[ ,"n"]
    } 
  }
  
  # Stats to display: 
  #   day of last activity: 
  last_seen <- subset(get_rawdata_table(), feeder == folder_df[ ,"feeder"])
  last_seen <- with(last_seen, as.Date(paste(year, month, day, sep  = "-")))
  last_seen <- max(last_seen)
  
  # Statistics on number of birds per day, number of birds tagged
  if ( okdf(all_birds) ) { 
    sumbird <- sum(bird_counts[ ,"n"], na.rm = TRUE)
    avbird <- mean(bird_counts[ ,"n"], na.rm = TRUE)
    total_tagged <- sum( !is.na(all_birds[ ,"species"]) )
    perc_tagged <- round(total_tagged / nrow(all_birds) * 100, digits = 1)
  } else { 
    sumbird <- avbird <- total_tagged <- perc_tagged <- 0
  }
  
  # Check if hasbird model is enabled 
  hasbird_mods <- hasbird_model_train()
  hasbird_mods <- laply(hasbird_mods, function(o) ! is.null(o[["mod_noweight"]]))
  feeders_with_hasbird <- names(hasbird_mods)[hasbird_mods]
  hasbird_model_enabled <- folder_df[ ,"feeder"] %in%  feeders_with_hasbird
  
  # Check if species model is enabled 
  species_model_enabled <- ! is.null({ 
    species_model_train(folder_df[ ,"feeder"])[[1]]
  })
  
  # Update feeders table with new data 
  datadf <- data.frame(feeder = folder_df[ ,"feeder"], 
                       birdn_today = n_today, 
                       birdn_total = sumbird, 
                       birdn_average = avbird, 
                       tags_n = total_tagged, 
                       tags_perc = perc_tagged, 
                       last_active = last_seen, 
                       hasbird_model_enabled = hasbird_model_enabled, 
                       species_model_enabled = species_model_enabled)
  update_feeder_data(datadf)
  
  
  
  # Start rendering of graphs (note: ideally this function should only create 
  # graphs)
  
  # If the feeder has seen birds, then also update the trend graphs 
  cum_trend_html <- ""
  season_trend_html <- ""
  if ( okdf(all_birds) && okdf(bird_counts) ) { 
    
    
    # Make graph of n by day
    ggobj <- seasonal_graph(bird_counts, what = "n", 
                            ylab = "Nombre d'oiseaux", 
                            maxgap = SEASON_TREND_MAXGAP, 
                            trendline = TRUE, 
                            trendline.args = list(family = poisson()))
    
    save_ggplot2_widget(ggobj, local_outdir, 
                        name_prefix = "seasontrend", 
                        width = LITY_WIDTH, 
                        height = LITY_HEIGHT, 
                        img_width = 800, 
                        img_height = 250, 
                        tooltip = "text")
    
    # Make graph of cumulative number of birds
    #TODO:to the cumulative graph by season
    cum_birds <- ddply(mutate(bird_counts, s = makeseason(year, month)), 
                       ~ s, mutate, cumn = cumsum(n))
    
    cumulative_graph <- seasonal_graph(cum_birds, "cumn", 
                                       ylab = "Nombre d'oiseaux\n(cumulé)",
                                       maxgap = SEASON_TREND_MAXGAP)
    
    save_ggplot2_widget(cumulative_graph, local_outdir, 
                        name_prefix = "seasoncumulative", 
                        width = LITY_WIDTH, 
                        height = LITY_HEIGHT, 
                        img_width = 800, 
                        img_height = 250, 
                        tooltip = "text")
    
    # Make a colored graph for the different species 
    species_counts <- ddply(all_birds, ~ spauto, count, "spauto")
    species_counts <- subset(species_counts, !is.na(spauto) & freq > 100)
    species_all_birds <- subset(all_birds, 
                                spauto %in% species_counts[ ,"spauto"] & 
                                  !is.na(spauto))
    species_all_birds <- ddply(mutate(species_all_birds, 
                                      year = year(datetime), 
                                      month = month(datetime), 
                                      day   = day(datetime)), 
                               ~ year + month + day + spauto, summarise, 
                               n = length(year))
    # For each day, if there is any bird, then set to zero all other 
    # birds
    allspp <- unique(species_all_birds[ ,"spauto"])
    species_all_birds <- ddply(species_all_birds, ~ year + month + day, 
                        function(o) { 
                          ref <- data.frame(spauto = allspp, 
                                            except(o, c("spauto", "n"))[1, ])
                          new <- join(ref, o, type = "left", match = "first", 
                                      by = names(ref))
                          new[is.na(new[ ,"n"]), "n"] <- 0
                          return(new)
                        })
    
    # If we could count birds, then make the graph
    if ( okdf(species_all_birds) ) { 
      species_graph <- seasonal_graph(species_all_birds, 
                                      what = "n", 
                                      groups = "spauto", 
                                      ylab = "Nombre d'oiseaux", 
                                      maxgap = SEASON_TREND_MAXGAP, 
                                      trendline = TRUE, 
                                     trendline.args = list(family = poisson())) + 
                      facet_wrap( ~ groups, ncol = 1, scales = "free_y")
      
      n_species <- length(unique(species_all_birds[ ,"spauto"]))
      save_ggplot2_widget(species_graph, local_outdir, 
                          name_prefix = "seasonspecies", 
                          width = LITY_WIDTH, 
                          height = LITY_HEIGHT, 
                          img_width = 800, 
                          img_height = 200 * n_species, 
                          tooltip = "text")
    } 
    
  }
  

  # Make the plots for birds
  if ( okdf(all_birds) ) { 
    gglist <- birds_season_heatmap(all_birds)
    for ( o in gglist ) { 
      nameprefix <- with(o, p("heatmap_", season, "_", sp_to_spcode(species)))
      save_ggplot2_widget(ggobj = o[["for_ggplot"]], 
                          plotly_gg = o[["for_plotly"]], 
                          local_outdir, 
                          name_prefix = nameprefix, 
                          width = LITY_WIDTH, 
                          height = LITY_HEIGHT, 
                          img_width = 800, 
                          img_height = 250, 
                          tooltip = "text")
    }
  }
  
  # Make the plots for temperature 

  # Make the plots for birds
  if ( okdf(all_birds) ) { 
      
    sql <- p("SELECT date, maxtemp, meantemp, mintemp", " ", 
            "FROM ", DBDAILYTBL, " WHERE enabled = '1' AND ", 
            "feeder = ?")
    con <- db_connect()
    req <- dbSendQuery(con, sql)
    dbBind(req, list(folder_df[1, "feeder"]))
    dailytemps <- dbFetch(req)
    
    ggobj <- temperature_annual_trend(dailytemps)
    save_ggplot2_widget(ggobj, local_outdir, 
                        name_prefix = "seasontemps", 
                        width = LITY_WIDTH, 
                        height = LITY_HEIGHT, 
                        img_width = 800, 
                        img_height = 400, 
                        tooltip = "text")
  }
  
  # Actually send the files 
  send_files(outpath, expand = TRUE, flush = TRUE)
  
  return(TRUE)
}

# Given year and month, returns a season such as '2018-2019'
makeseason <- function(years, months, month_cut = 7) { 
  ifelse(months <= month_cut, 
         paste(years - 1, years, sep = "-"), 
         paste(years, years + 1, sep = "-"))
}

birds_season_heatmap <- function(birdstbl) { 
  
  birdstbl[ ,"species_merge"] <- with(birdstbl, 
                                    ifelse(is.na(species), 
                                           species_auto, species))
  
  # Summarise hour-by-hour
  counts <- mutate(birdstbl, hour = lubridate::hour(datetime), 
                   date = as.Date(datetime))
  counts <- ddply(counts, ~ species_merge + date + hour, summarise, 
                  n = sum(model_species_pred > 0.8 | is.na(model_species_pred)))

  # Keep only abundant species
  tcounts <- ddply(birdstbl, ~ species_merge, summarise, n = length(species))
  tcounts <- subset(tcounts, n > 50 & !is.na(species_merge))
  counts <- subset(counts, species_merge %in% tcounts[ ,"species_merge"])
  
  # We can subset and end up with a data.frame with 0 rows
  if ( ! okdf(counts) ) { 
    return(NULL)
  }
  
  # Make sure all species absent on observed hours are set to zero
  counts <- complete(counts, c("species_merge", "date", "hour"), 
                     fill = list(n = 0))

  # Extend the data.frame to hold values for all days of the year. 
  # This is needed for plotly to work correctly. 
  year_table <- expand.grid(hour = seq(MIN_HOUR, MAX_HOUR), 
                            # We go to last year + 1 to handle last season
                            year = seq(min(year(counts[ ,"date"])), 
                                      max(year(counts[ ,"date"])+1)), 
                            month = seq(1, 12), 
                            day = seq(1, 31), 
                            species_merge = unique(counts[ ,"species_merge"]))

  ecounts <- join(year_table, 
                  mutate(counts, year = year(date), month = month(date), 
                        day = mday(date)), 
                  by = names(year_table))
  ecounts <- mutate(ecounts, 
                    date = as.Date(paste(year, month, day, sep = "-")))
                    
  # Prepare for seasonal graph
  counts <- ddply(ecounts, ~ species_merge, prepare_seasonal_data, "date", 
                  groups = "hour")




  # Add all days of the year 
  # This is needed to have ggplotly work correctly

  # Sort species by abundance
  lvls <- rev(arrange(tcounts, n)[ ,"species_merge"])
  counts[ ,"species_merge"] <- factor(counts[ ,"species_merge"], 
                                      levels = lvls)

  # Get dawn/dusk times 
  suntimes <- suncalc::getSunlightTimes(counts[ ,"date"], 
                                        lat = 46.573967 , lon = 2.504883)
  counts <- mutate(counts, 
                  is_night = hour <= hour(suntimes[ ,"dawn"]) | 
                              hour >= hour(suntimes[ ,"night"]))

  counts <- mutate(counts, 
                  hourf = factor(hour, 
                                  levels = rev(seq(MIN_HOUR, MAX_HOUR))))
  
  all_plots <- dlply(counts, ~ season + species_merge, function(df) { 
    count_subs <- 
      subset(df, 
             complete.cases(species_merge, hourf, dayofyear_shift) & 
               (month > MIN_MONTH | month < MAX_MONTH))
    
    # If we do not have enough counts, then bail 
    if ( sum(count_subs[ ,"n"] > 0, na.rm = TRUE) < 50 ) { 
      return(NULL)
    }
    
    # Make discrete intervals for counts
    count_subs[ , "ncut"] <- cut(count_subs[ ,'n'], 
                                c(-Inf, 0, 1, 2, 5, 10, Inf))
    colors <- c("#FCFCFC", "#FFFFBF", "#FFF387", "#FDAE61", "#D7191C", 
                "#991236")
    names(colors) <- levels(count_subs[ ,"ncut"])
    
    brks <- subset(seasonal_breaks(return_scale=FALSE), 
                  month > 8 | month < 6)
    
    a <- ggplot(count_subs, 
                aes(x = dayofyear_shift, y = hourf)) + 
      geom_tile(aes(fill = ncut, width = 1, 
                    text = ifelse(is.na(n), 
                                  "Pas de données", 
                                  p(date, " - ", hourf, "h\n", 
                                    n, " visites/h")))) +  
      seasonal_breaks() + 
      scale_y_discrete(labels = function(x) { 
          ifelse(as.numeric(x) %% 2 == 0, p(x, "h"), "")
        }) + 
      scale_x_continuous(name = "", 
                         breaks = brks[ ,"shiftval"], 
                         labels = brks[ ,"month_string"], 
                         limits = range(brks[ ,"shiftval"])) + 
      scale_fill_manual(values = colors, 
                        name = "Visites", 
                        na.value = "#EEEEEE") + 
      labs(y = "") + 
      theme(legend.position = "none") 
    
    b <- a + 
      geom_tile(data = subset(count_subs, is_night), 
                fill = "black", alpha = .2) 
    
    list(for_plotly = a, 
         for_ggplot = b, 
         species = df[1, "species_merge"], 
         season = df[1, "season"])
  })

  all_plots <- Filter(function(x) ! is.null(x), all_plots)
  
  return(all_plots)
}

temperature_annual_trend <- function(temps) { 
  
  # Temperature throughout the year 
  temps <- prepare_seasonal_data(temps, "date", maxgap = 2)
  temps <- gather(temps, "temp", "value", c("maxtemp", "meantemp", "mintemp"))
  temps[ ,"temp"] <- c(maxtemp = "Max.", meantemp = "Moyenne", 
                       mintemp = "Min.")[ temps[ ,"temp"] ]
  temps <- subset(temps, month > MIN_MONTH | month < MAX_MONTH)
  
  ggplot(temps, aes(x = dayofyear_shift)) + 
    geom_rect(aes(ymin = pmin(value, 0), 
                  ymax = rep(0, length(value)), 
                  xmin = dayofyear_shift - 0.5, 
                  xmax = dayofyear_shift + 0.5), 
              fill = "#ABDECE", alpha = .3, 
            data = subset(temps, temp == "Min.")) + 
    geom_hline(aes(yintercept = y), data = data.frame(y = 0), 
              linetype = "dashed") + 
    geom_line(aes(y = value, color = temp, group = p(gaps, temp), 
                  text = p(date, ": ", "T. ", temp, " " , 
                          sprintf("%2.1f", value), "°C")), 
              size = 0.5) + 
    lims(y = c(min(c(0, temps[ ,"value"])), 
               max(temps[ ,"value"]))) + 
    scale_color_manual(values = c("#D05301", "#01A4D0", "#9F9F9F"), 
                       name = "Température") + 
    scale_fill_gradient2(low = c("#ABDECE"), 
                         high = c("#FC8D62"), 
                         midpoint = 0, 
                         name = "Température", 
                         guide = "none") + 
    seasonal_breaks() + 
    theme(legend.position = "bottom", 
          legend.direction = "horizontal", 
          legend.justification = 0, 
          panel.grid.major.x = element_blank(), 
          panel.grid.minor.x = element_blank()) + 
    labs(x = "", 
        y = "Température (°C)")
}
