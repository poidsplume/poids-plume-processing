#
# Copyright (2020), le Projet Poids Plume 
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 

# 
# Perform some checks on what is available on the host system
# 

check_command <- function(cmd) { 
  if ( system(cmd, ignore.stdout = TRUE, ignore.stderr = TRUE) == 127 ) { 
    stop('Could not find the following command: ', cmd)
  }
  return(TRUE)
}

check_command("rsync")
check_command("ffmpeg")
check_command("MP4Box")
check_command("optipng")
check_command("mogrify")

# Import fonts and set ggplot2 fonts
extrafont::font_import("./tools/", prompt = FALSE)
