#
# Copyright (2020), le Projet Poids Plume 
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 

# 
# This file contains code that will render a daily html summary
# 


render_rawdata <- function(workers = WORKERS, ...) { 
  renderer(get_processed_table(), 
           fun = render_one_rawdata, 
           message = expression(p('Rendering raw data: ', folder, '\n')), 
           workers = workers, 
           ...)
  return(TRUE)
}

render_one_rawdata <- function(folder_df) { 
  
  # Get paths where to produce html/figures, etc. 
  subdir <- with(folder_df, 
                 fpath("generated/rawdata/", feeder, 
                       paste(year, lzero(month), lzero(day), 
                             sep = "-")))
  local_outdir <- fpath(WEBOUTPUT_LOCALFOLDER, subdir)
                       
  # Create directories for the html file 
  if ( ! exists(local_outdir) ) { 
    dir.create(local_outdir, recursive = TRUE)
  } 
  
  # Load data 
  filtdat <- read_featherdf(p(slash(folder_df[['folder']]), "filtdat.fdat"))
  
  # CHeck if all columns are na
  NAcols <- apply(filtdat[ ,dataref[ ,"datatype"]], 2, 
                  function(x) all(is.na(x)))
  if ( all(NAcols) ) { 
    return(FALSE)
  }
  
  # Make raw graphs 
  all_raw_graphs_html <- 
    map_chr(dataref[ ,'datatype'], 
            function(n) { 
              a <- make_trend_plot(filtdat, n, local_outdir)
              return( ifelse(is.na(a), NA, urlpath(subdir, a)) )
            })
  noNA <- !is.na(all_raw_graphs_html) # Remove missing data
  all_raw_graphs_html <- all_raw_graphs_html[noNA]
  
  # Make weight debug trend and send it to server 
  # Get bird data for that day 
  sql <- p("SELECT datetime AS time, birdweight FROM ", DBVIDEOSTBL, " WHERE ", 
           "date = '", with(folder_df, paste(year, month, day, sep = "-")), "'")
  con <- db_connect()
  birds <- dbFetch(dbSendQuery(con, sql))
  make_weight_raw_graph(filtdat, birds, local_outdir)
  
  # Send graphs to server
  send_files(subdir, expand = TRUE)
  
  update_df <- data.frame(pagetype = "rawdata", 
                          feeder = folder_df[ ,"feeder"], 
                          date = with(folder_df, 
                                      as.Date(p(year, month, day, sep = "-"))), 
                          category = "rawdata_graphs", 
                          datakey = dataref[noNA, "datatype"], 
                          description = dataref[noNA, "name"], 
                          val_str = all_raw_graphs_html, 
                          val_int = dataref[noNA, "order"])
  
  update_page_data(update_df)
  
  return(TRUE)
}

# Make a trend plot and save the graph in the 
make_trend_plot <- function(data, col, outdir, asbox = TRUE) { 
  
  # Warn if no corresponding column in `data`
  if ( ! col %in% names(data) ) { 
    cat(col, "is not is the recorded data! Not rendering it.\n")
    return(NA)
  }
  
  data_info <- subset(dataref, datatype == col)
  
  # Bail if all missing
  if ( all(is.na(data[ ,col])) ) { 
    return(NA)
  }
  
  # Build ggplot object
  trend_plot <- trendplot(col, 
                          with(data_info, p(name, " (", unit, ")")), 
                          data)
  
  # Save image
  plot_basename <- p(col, "_plot")
  imgplotfile <- p(plot_basename, '.png')
  ggsave_optim(trend_plot, 
               filename = fpath(outdir, imgplotfile), 
               width = HTMLOPTS$raw_timeseries_image_width, 
               height = HTMLOPTS$raw_timeseries_image_height)
  
  # Save plotly
  plotfile <- p(plot_basename, '.html')
  save_plotly(trend_plot, p(slash(outdir), plotfile))
  
  return(plot_basename)
}

make_weight_raw_graph <- function(data, birds, outdir) { 
  data[ ,"hour"] <- hour(num_to_posixt(data$time))
  birds[ ,"hour"] <- hour(num_to_posixt(birds$time))
  datasub <- subset(data, hour > 6 & hour < 22)
  
  # Make base plot
  ggobj <- ggplot(datasub, aes(x = num_to_posixt(time))) + 
    geom_line(aes(y = weight), size = 0.1) + 
    geom_line(aes(y = wgtlow), size = 0.1, color = "red") +
    facet_wrap( ~ hour %/% 2, scales = "free_x", ncol = 1) + 
    theme(strip.background = element_blank(),
          strip.text.x = element_blank(), 
          text = element_text(family = "sans"))
  
  # Add bird info if present 
  if ( okdf(birds) ) { 
    ggobj <- ggobj + 
      geom_segment(aes(x = num_to_posixt(time), 
                       xend = num_to_posixt(time), 
                       y = -Inf, 
                       yend = Inf), 
                   alpha = .5, linetype = "dashed", size = .1, 
                   data = birds) + 
      geom_text(aes(x = num_to_posixt(time), 
                    y = -Inf, 
                    label = p(round(birdweight, 1), "g")), 
                data = birds, size = 1, angle = 90, vjust = 0, hjust = 0)
  }
  
  nrows <- length(unique(data[ ,"hour"] %/% 2))
  ggsave_optim(ggobj, 
               filename = fpath(outdir, "weight_full_trend.pdf"), 
               width = HTMLOPTS$raw_timeseries_image_width * 3, 
               height = HTMLOPTS$raw_timeseries_image_height * nrows, 
               limitsize = FALSE)
  
  return(TRUE)
}

# Create a time-series in ggplot2
trendplot <- function(what, ylab, dat) { 
  subset_factor <- subset(dataref, datatype == what)[ ,"graph_subset_factor"]
  dat <- subset(dat, seq.int(nrow(dat)) %% subset_factor == 0)
  dat$htime <- num_to_posixt(dat$time)
  dat <- subset(dat, hour(htime) > 4 & hour(htime) < 23)
  dat$y <- dat[ ,what]
  
  ggplot(dat) + 
    geom_line(aes_string(x = "htime", y = "y"), 
              size = 0.1) + 
    labs(x = "Time", y = ylab)
}

# Save trendplot as a plot.ly html widget 
save_plotly <- function(ggobj, file, ...) { 
  plotly_obj <- ggplotly(ggobj, ...)
  # For some reason we cannot directly write to the new file, so we use 
  # a temporary file that we copy there
  save_raw_ts_widget(widget = plotly_obj, 
                     selfcontained = TRUE, 
                     file = file)
  return(TRUE)
}

