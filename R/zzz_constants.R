# 
# Define some constants for reuse
# 


ALLMONTHS_ABBR <- c("Jan", "Fev", "Mar", "Avr", "Mai", "Juin", "Juil", 
                    "Aou", "Sep", "Oct", "Nov", "Dec")
ALLDAYS_ABBR <- c("Lun.", "Mar.", "Mer", "Jeu.", "Ven.", "Sam.", "Dim.")
                  
MODEL_FULLNAMES <- list(species_noweight = "Espèces (sans masse)", 
                        species_weight   = "Espèces (avec masse)", 
                        hasbird_noweight = "Présence d'un oiseau")

