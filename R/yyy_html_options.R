#
# Copyright (2020), le Projet Poids Plume 
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
# 

# 
# These are html options that we need to define globally as R needs to be 
# aware of them. 
# 

HTMLOPTS <- list(gauge_width = 200,      # as numeric, but in px
                 gauge_height = 150,     # as numeric, but in px
                 longgraph_width = 500,  # as numeric, but in px
                 longgraph_height = 160, # as numeric, but in px
                 raw_timeseries_width = 964, # as numeric, but in px
                 raw_timeseries_height = 542, # as numeric, but in px
                 # These defined the height/width of images of raw plots.
                 raw_timeseries_image_width = 9, # as inches
                 raw_timeseries_image_height = 3) # as inches
                 
LITY_WIDTH <- 964
LITY_HEIGHT <- 542
