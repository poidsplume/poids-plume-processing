// 
// File that contain helpers for remove_hx711_outliers
// 

#include <RcppArmadillo.h>
using namespace arma;

//[[Rcpp::export]]
arma::vec remove_small_stretches(arma::vec rleval, 
                                 arma::vec rlelen, 
                                 double wgtmedian) { 
  for ( uword i=0; i<(rleval.n_elem - 1); i++ ) { 
    if ( rlelen(i) < 10 && 
         rleval(i) != NA_REAL && 
         rleval(i) < (wgtmedian - 5) ) { 
      rleval(i) = rleval(i+1); 
    }
  }
  
  return(rleval); 
}
