// 
// pkgbuild::with_debug(load_all(), debug = FALSE)
// 
// Count uniq values within a moving window
//

#include <RcppArmadillo.h>
#include <math.h>

using namespace Rcpp; 
using namespace arma; 

//[[Rcpp::export]]
arma::uvec count_uniq_vals(arma::vec ws, 
                           arma::vec obsw, 
                           arma::uvec obscounts, 
                           double winwidth) { 
  
  // Init counts vector
  arma::uvec newcounts(ws.n_elem); 
  newcounts.zeros(); 
  
  // For each input weight
  // We know that the ws are in ascending order. So after ending an iteration 
  // of the obsws, we do not need to check the values that are below the 
  // first match in obsw. 
  uword minobsi = 0; 
  for (uword wi=0; wi<ws.n_elem; wi++) { 
    bool is_first_match = true; 
    // We go over the weight count values
    for ( uword obsi=minobsi; obsi<obsw.n_elem; obsi++ ) { 
      bool is_in_window = fabs(obsw(obsi) - ws(wi)) < winwidth; 
      // If the values of weights are in the window
      if ( is_in_window ) { 
        newcounts(wi) += obscounts(obsi);
        // If this is the first match, the next iteration (for the next ws 
        // value will start from there)
        if ( is_first_match ) { 
          minobsi = obsi; 
          is_first_match = false; 
        }
      }
    }
  }
  
  return(newcounts); 
}
