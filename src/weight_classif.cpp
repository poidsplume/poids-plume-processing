//
// Copyright (2020), le Projet Poids Plume 
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.
// 

// 
// pkgbuild::with_debug(load_all(), debug = FALSE)
// 
// Count uniq values within a moving window
//

#include <RcppArmadillo.h>

using namespace Rcpp; 
using namespace arma; 

#define LOW 0
#define HIGH 1

// Weight has to be between these bounds to define the high state
#define HIGH_MIN 4
#define HIGH_MAX 300

//[[Rcpp::export]]
arma::uvec weight_classif_cpp(arma::vec wgtin, 
                              arma::uword HZ, 
                              arma::vec wgt_baseline) { 
  
  // Define some constants
  uword backlog_n = HZ * 6 * 60; 
  uword minhigh_n = HZ * 1; 
  uword timeout   = HZ * 10 * 60; 
  
  uword N = wgtin.n_elem; 
  arma::uvec ishigh(N); 
  ishigh.zeros(); 
  
  uword current_state = LOW; 
  uword current_statechange_n = 0; 
  uword last_ix_in_low_stretch = 0; 
  
  // We scan the time series starting from the backlog_n^th value. 
  uword ix = backlog_n; 
  while ( ix < N ) { 
    // Rcout << ix - backlog_n << " to " << ix << "\n"; 
    
    // Check for NAs in back selection
    bool hasNA = false; 
    uword k = ix; 
    while ( k > (ix-backlog_n) ) { 
      if ( R_IsNA(wgtin(k)) ) { 
        hasNA = true; 
        // Rcout << "found NA at " << k << "\n"; 
        break;
      }
      k--; 
    }
    // We fast forward ix so that the next back selection does not include the 
    // NA value. 
    if ( hasNA ) { 
      ix = k + backlog_n + 1; 
      continue;
    }
    
    
    if ( current_state == LOW ) { 
      // Weight is at least 5g above the baseline, and below 300g. And we are not 
      // for too long in the high state. 
      if ( wgtin(ix) > (wgt_baseline(ix) + HIGH_MIN) && 
           wgtin(ix) < (wgt_baseline(ix) + HIGH_MAX) && 
           (ix - last_ix_in_low_stretch) <= timeout ) { 
        current_statechange_n++; 
      } else { 
        // Not in high state
        current_statechange_n = 0; 
        last_ix_in_low_stretch = ix; 
      }
    // Current state is HIGH
    } else {  
      // But we have low weight. We switch immediately to low weight state. 
      if ( wgtin(ix) < (wgt_baseline(ix) + HIGH_MIN) ) { 
        current_statechange_n = minhigh_n + 1; 
        last_ix_in_low_stretch = ix; 
      } 
    }
    
    // We need to change the state
    if ( current_statechange_n > minhigh_n ) { 
      //Rcout << "ix:" << ix << 
      //  " baseline:" << wgt_baseline(ix) << 
      //  " currentw:" << wgtin(ix) << 
      //  " last_low:" << last_ix_in_low_stretch << 
      //  " state:" << current_state << "\n"; 
      
      // We change the state. If we are switching to the high state, then 
      // we also need to write the last minhigh_n values as high state. 
      current_state = current_state == LOW ? HIGH : LOW;
      if ( current_state == HIGH ) { 
        for ( uword i=ix-minhigh_n; i<ix; i++ ) { 
          ishigh(i) = current_state; 
        }
      }
      current_statechange_n = 0; 
    }
    
    // Write down the current state
    ishigh(ix) = current_state; 
    
    Rcpp::checkUserInterrupt(); 
    ix++; 
  }
  
  return( ishigh ); 
}
