#!/bin/bash
# 
# 
set -euf -o pipefail

# We load the secrets file which is not checked in git
SECRET_FILE="$(dirname "$(realpath "$0")")/scripts.secrets"
if [ -e "$SECRET_FILE" ]; then 
  source "$SECRET_FILE"
else 
  echo "No secrets provided." 
  exit 1
fi

echo "Creating tunnel to mysql server"
if [ "$(lsof -i -P -n | grep -c "$LOCAL_MYSQL_PORT (LISTEN)")" == "0" ]; then 
  ssh -CN "$REMOTE_USER@$REMOTE_SERVER" \
    -L "$LOCAL_MYSQL_PORT":localhost:"$REMOTE_MYSQL_PORT" &  
  EXITCODE=$? 
  echo "$$" > "$SSH_PIDFILE"
else 
  echo "Warning: ssh tunnel for mysql already active"
  EXITCODE=0
fi





if [ "$(echo "$RUN_ON_HOSTS" | grep -c "$(hostname)")" == "0" ]; then 
  echo "This script should not run further on the current host"
  exit 0
fi


# echo "Mounting remote server web directory as sshfs..."
# if [ "$(mount | grep -c "$REMOTE_FOLDER")" -eq 0 ]; then 
#   sshfs \
#     -o "IdentityFile=$SSH_KEYFILE" \
#     -o "reconnect" \
#     -o "ServerAliveInterval=15" \
#     -o "auto_cache" \
#     -o "Compression=no" \
#     "$REMOTE_USER@$REMOTE_SERVER:$REMOTE_FOLDER" \
#     "./web_output"
#   EXITCODE=$?
# else
#   echo "Warning: remote server directory already mounted"
#   EXITCODE=0
# fi

if [ ! -d "$TARGET_DIR" ]; then 
  mkdir -p "$TARGET_DIR"
fi

# if [ "$(df "./web_output" | grep -c "web_output")" -eq 0 ]; then 
#   echo "Mounting target directory as bind mount..."
#   sudo mount --bind "$TARGET_DIR" "./web_output"
#   EXITCODE=$?
# else
#   echo "Warning: target directory already mounted"
#   EXITCODE=0
# fi

exit $EXITCODE

