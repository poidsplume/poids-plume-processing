
<?php 

require_once "<%= slash(SITE_ROOT_ON_SERVER) %>require_login.php";

if ( isset($_GET) && isset($_GET["code"]) ) { 
  $user_code = trim($_GET["code"]); 
} else { 
  echo "Undefined user code."; 
  die(); 
}

if ( empty(trim($user_code)) ) { 
  echo "Empty user code."; 
  die(); 
}

if ( $_SERVER["REQUEST_METHOD"] == "POST" ) {
  echo "Not implemented yet"; 
  die(); 
} 

$name = $family_name = $username = $created_at = ""; 

if ( $_SERVER["REQUEST_METHOD"] == "GET" ) { 
  // Make connection to database
  require_once "dbconfig.php";

  $sql = "SELECT name, family_name, username, created_at FROM userdata WHERE approval_key = ?";
  
  if ( $stmt = mysqli_prepare($link, $sql) ) {
    
    // Bind variables to the prepared statement as parameters
    mysqli_stmt_bind_param($stmt, "s", $param_user_code);
    $param_user_code = $user_code; 
    
    // Attempt to execute the prepared statement
    if ( mysqli_stmt_execute($stmt) ) {
      
      // Store result
      mysqli_stmt_store_result($stmt);
      
      // Check if a corresponding user exists, if yes then proceed
      if ( mysqli_stmt_num_rows($stmt) == 1 ) { 
        
        // Bind result variables
        mysqli_stmt_bind_result($stmt, $name, $family_name, $username,
                                $created_at); 
        
        if ( mysqli_stmt_fetch($stmt) ) {
          // Nothing to do on success, vars are assigned
        } else { 
          echo "Could not fetch results.";
          die(); 
        }
      } else {
        // Display an error message if username doesn't exist
        echo "Mauvaise clé d'activation.";
        die(); 
      }
    } else {
      echo "Un problème est survenu, merci d'essayer plus tard.";
      die(); 
    }
  } else { 
    echo "Error preparing request."; 
    die(); 
  }
  
}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <link href="<%= slash(SITE_ROOT) %>static/main.css" rel="stylesheet"
        type="text/css">
  
  <meta charset="UTF-8">
  <title>Poids-Plume | Connexion</title>
  <style type="text/css">
    .wrapper { width: 350px; padding: 20px; }
  </style>
  <!-- Do not index this page. -->
  <meta name="robots" content="noindex">
</head>
  
<body>

<h1>Valider une inscription</h1>

  <div class="wrapper">
  
    <h2>Valider une inscription</h2>
    
    <ul>
      <li>Nom d'utilisateur: <?php echo $username ?></li>
      <li>Prénom: <?php echo $family_name ?></li>
      <li>Nom de famille: <?php echo $name ?></li>
      <li>Créé le: <?php echo $created_at ?></li>
    </ul>
    
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>"
          method="post">
      <input type="submit" value="Valider le compte">
    </form>
    
  </div>
  
</body>

</html>
